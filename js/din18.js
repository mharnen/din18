//~~~~~~~~~~~~INTRO SLIDE~~~~~~~~~~~~~~~~~~~

function intro_1(){
  $("#intro_input").delay(500)
    .velocity({translateX: 35, translateY: 50}, 1500);
}

function intro_2(){
  intro_result.style.visibility="visible";
  $("#intro_result").delay(500)
    .velocity({translateX: 0, translateY: -50}, 1500);
}

function intro_3(){
  intro_yes.style.visibility="visible";
}
function intro_4(){
  $("#intro_money").delay(500)
    .velocity({translateX: 18, translateY: 49}, 1500);
}

//~~~~~~~~~~~~PAYMENTS SLIDE~~~~~~~~~~~~~~~~~~~

function payments_1(){
  payments_hash.style.visibility="visible";
  payments_arrow.style.visibility="visible";
}

function payments_2(){
  payments_arrow.style.visibility="hidden";
  $("#payments_money").delay(1500)
    .velocity({translateX: 75, translateY: 32}, 1500);
  $("#payments_deposit").delay(1000)
      .velocity({translateX: 75, translateY: 32}, 1500);
  $("#payments_hash")
        .velocity({translateX: 75, translateY: 32}, 1500);
  $("#payments_function")
        .velocity({translateX: 80, translateY: 10}, 1500);
}

function payments_3(){
  payments_deposit2.style.visibility="visible";
  $("#payments_deposit2").delay(1000)
    .velocity({translateX: 68, translateY: -28}, 1500);
}

function payments_4(){
  payments_animateGear1_1.beginElement();
  payments_enclave.style.visibility="visible";
}

function payments_5(){
  payments_tls.style.visibility="visible";
}

function payments_6(){
  payments_question.style.visibility="visible";
  $("#payments_question").delay(1500)
    .velocity({translateX: 2, translateY: 48}, 2000)
    .velocity({ opacity: 0 }, {duration: 500}, {visibility: "hidden"});
}

function payments_7(){
  payments_yes.style.visibility="visible";
  $("#payments_yes").delay(1500)
    .velocity({translateX: -2, translateY: -48}, 2000)
    .velocity({ opacity: 0 }, {duration: 500}, {visibility: "hidden"});
}

function payments_8(){
  payments_data.style.visibility="visible";
  $("#payments_data").delay(500)
    .velocity({translateX:-2, translateY: 61}, 2000);
  $("#payments_key").delay(2000)
    .velocity({translateX: 0, translateY: 65}, 2000);

}

function payments_9(){
  payments_result.style.visibility="visible";
  $("#payments_result").delay(500)
    .velocity({translateX:15, translateY: -10}, 2000);
  $("#payments_key").delay(2500)
      .velocity({translateX: 22, translateY: 60}, 2000);
}

function payments_10(){
  payments_animateGear1_1.endElement();
  payments_tls.style.visibility="hidden";
  payments_data.style.visibility="hidden";
  payments_enclave.style.visibility="hidden";
  $("#payments_key")
    .velocity({translateX: 85, translateY: 45}, 2000)
    .velocity({ opacity: 0 }, {duration: 500}, {visibility: "hidden"});
  $("#payments_deposit2").delay(2500)
      .velocity({translateX: 0, translateY: 0}, 2000)
}

function payments_11(){
  $("#payments_result")
      .velocity({translateX: 15, translateY:-70}, 2000)
}

function payments_12(){
  //payments_yes.style.visibility="visible";
  $("#payments_yes")
    .velocity({translateX: 30, translateY: -45}, 0)
    .velocity({ opacity: 1 }, {duration: 0}, {visibility: "visible"})
    .delay(500)
    .velocity({translateX: 85, translateY: -25}, 2000)
    .velocity({ opacity: 0 }, {duration: 500}, {visibility: "hidden"});
}

function payments_13(){
  $("#payments_deposit")
      .velocity({translateX: -10, translateY: 0}, 1500);
  $("#payments_money").delay(2000)
          .velocity({translateX: 17, translateY: 70}, 1500);
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~ INTEL SGX SLIDE~~~~~~~~~~~~~~~~
function sgx_1(){
  $("#key4_2")
    .velocity({translateX: 10, translateY: -50}, 2000);
}

function sgx_2(){
  $("#gear4_2")
    .velocity({translateX: 0, translateY: -15}, 1000)
    .velocity({translateX: 75, translateY: -15}, 1500)
    .velocity({translateX: 75, translateY: 0}, 1000);

    setTimeout(switchGears, 3500);
}

function switchGears(){
  gear4_2.style.visibility="hidden";
  gear4_3.style.visibility="visible";
}

function sgx_3(){
  animateGear4_3.beginElement();
  enclave4_1.style.visibility="visible";
}

function sgx_4(){
  $("#key4_1")
    .velocity({translateX: 0, translateY: -5}, 500)
    .velocity({translateX: 75, translateY: -5}, 2000);
}

function sgx_5(){
  secret4_1.style.visibility="visible";
  setInterval(movePackets4, 4000);
}

function movePackets4(){
  $("#secret4_1")
    .velocity({translateX: 0, translateY: 0}, 0)
    .velocity({opacity: 1}, { duration: 500 }, {visibility: "visible"})
    .velocity({translateX: 10, translateY: 0}, 1000)
    .velocity({translateX: 10, translateY: 45}, 1800)
    .velocity({ opacity: 0 }, {duration: 500}, {visibility: "hidden"});
}
