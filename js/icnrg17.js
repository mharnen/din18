//~~~~~~~~~~~~SIGNATURES SLIDE~~~~~~~~~~~~~~~~~~~
function icnrg2_1(){
  icnrg2_packet2_1.style.visibility="visible";

  $("#icnrg2_packet2_1")
    .velocity({translateX: 40, translateY: -15}, 1500)
    .velocity({translateX: 40, translateY: -50}, 1500)
    .velocity({ opacity: 0 }, {duration: 1000}, {visibility: "hidden"});

  setTimeout(icnrg2_rejectPacket, 4000);
  setTimeout(icnrg2_addPIT, 1500);
}

function icnrg2_rejectPacket(){
  icnrg2_no2_1.style.visibility="visible";
  $("#icnrg2_no2_1").velocity("callout.flash")
  .delay(1000)
  .velocity({ opacity: 0 }, {duration: 1000}, {visibility: "hidden"});
}

function icnrg2_addPIT(){
  $("#icnrg2_pitEntry").text("/exec/A     3        1");
}

function icnrg2_2(){
  icnrg2_packet2_2.style.visibility="visible";

  $("#icnrg2_packet2_2")
    .velocity({translateX: -40, translateY: -15}, 1500)
    .velocity({ opacity: 0 }, {duration: 1000}, {visibility: "hidden"});
    setTimeout(icnrg2_addPIT2, 1500);
}

function icnrg2_addPIT2(){
  $("#icnrg2_pitEntry").text("/exec/A     3        1,2");
}

//~~~~~~~~~~~~~~~~~~~~~~~SIGNATURE CAPTURE SLIDE
function icnrg3_1(){
  icnrg3_packet3_2.style.visibility="visible";

  $("#icnrg3_packet3_2")
    .velocity({translateX: -40, translateY: -15}, 1500)
    .velocity({translateX: -40, translateY: -50}, 1500)
    .velocity({ opacity: 0 }, {duration: 1000}, {visibility: "hidden"});

  setTimeout(icnrg3_secondPacket, 1500);
}


function icnrg3_secondPacket(){
  icnrg3_packet3_1.style.visibility="visible";

    $("#icnrg3_packet3_1")
      .velocity({translateX: -40, translateY: 15}, 1500);
}

function icnrg3_2(){
  $("#icnrg3_packet3_1")
    .velocity({translateX: 0, translateY: 0}, 1500)
    .velocity({translateX: 0, translateY: -35}, 1500)
    .velocity({ opacity: 0 }, {duration: 1000}, {visibility: "hidden"});
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~ INTEL SGX SLIDE~~~~~~~~~~~~~~~~
function icnrg4_1(){
  $("#icnrg4_key4_2")
    .velocity({translateX: 10, translateY: -50}, 2000);
}

function icnrg4_2(){
  $("#icnrg4_gear4_2")
    .velocity({translateX: 0, translateY: -15}, 1000)
    .velocity({translateX: 75, translateY: -15}, 1500)
    .velocity({translateX: 75, translateY: 0}, 1000);

    setTimeout(icnrg4_switchGears, 3500);
}

function icnrg4_switchGears(){
  icnrg4_gear4_2.style.visibility="hidden";
  icnrg4_gear4_3.style.visibility="visible";
}

function icnrg4_3(){
  icnrg4_animateGear4_3.beginElement();
  icnrg4_enclave4_1.style.visibility="visible";
}

function icnrg4_4(){
  $("#icnrg4_key4_1")
    .velocity({translateX: 0, translateY: -5}, 500)
    .velocity({translateX: 75, translateY: -5}, 2000);
}

function icnrg4_5(){
  icnrg4_secret4_1.style.visibility="visible";
  setInterval(icnrg4_movePackets4, 4000);
}

function icnrg4_movePackets4(){
  $("#icnrg4_secret4_1")
    .velocity({translateX: 0, translateY: 0}, 0)
    .velocity({opacity: 1}, { duration: 500 }, {visibility: "visible"})
    .velocity({translateX: 10, translateY: 0}, 1000)
    .velocity({translateX: 10, translateY: 45}, 1800)
    .velocity({ opacity: 0 }, {duration: 500}, {visibility: "hidden"});
}
