//~~~~~~~~~~~~~~~~~~SLIDE 3~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

var animate1ID;
var animate2ID;
var clockAnimateID;
var packetInterval=700;

function exec_1(){
	exec_packet1.style.visibility="visible";
	exec_node1.style.fill="green";
	exec_movePacket1();
	animate1ID = setInterval(exec_movePacket1, 5000);
}

function exec_2(){
	exec_packet2.style.visibility="visible";
	exec_node11.style.fill="green";
	exec_movePacket2();
	animate2ID = setInterval(exec_movePacket2, 6000);
}

function exec_3(){
	exec_animateGear1.setAttribute('dur', '1s');
	exec_node2.style.fill="red";
	//		gear1.style.animationDuration = "1s";
}

function exec_4(){
	exec_clock1.style.visibility="visible";
	exec_clock2.style.visibility="visible";
	clockAnimateID = setInterval(function() {
		$("#exec_clock2").velocity("callout.flash");
	}, 2000);
}

function exec_stop(){
	clearInterval(animate1ID);
	clearInterval(animate2ID);
	clearInterval(clockAnimateID);
	exec_animateGear1.setAttribute('dur', '4s');
	exec_node2.style.fill="white";
	exec_node1.style.fill="white";
	exec_node11.style.fill="white";
	exec_clock1.style.visibility="hidden";
	exec_clock2.style.visibility="hidden";
}

function exec_movePacket1(){
	console.log("Time: " + packetInterval);
	$("#exec_packet1")
		.velocity({translateX: 0, translateY: 0}, 500)
		.velocity({opacity: 1}, { duration: 1000 }, {visibility: "visible"})
		.velocity({translateX: 21, translateY: 0}, packetInterval)
		.velocity({translateX: 48, translateY: 13}, packetInterval)
		.velocity({translateX: 80, translateY: 7}, packetInterval)
		.velocity({ opacity: 0 }, {duration: 500}, {visibility: "hidden"});

}

function exec_movePacket2(time){
	$("#exec_packet2")
		.velocity({translateX: 0, translateY: 0}, 500)
		.velocity({opacity: 1}, { duration: 1000 }, {visibility: "visible"})
		.velocity({translateX: 17, translateY: 0}, packetInterval)
		.velocity({translateX: 47, translateY: 1}, packetInterval)
		.velocity({translateX: 73, translateY: -18}, packetInterval)
		.velocity({translateX: 83, translateY: -44}, packetInterval)
		.velocity({translateX: 83, translateY: -74}, packetInterval)
		.velocity({ opacity: 0 }, {duration: 500}, {visibility: "hidden"});
}

//~~~~~~~~~~~~~~~~~~SLIDE 2~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

var animate1ID;
var animate2ID;
var animateBarID;

function animate2_1(){
	packet2_1.style.visibility="visible";
	node2_1.style.fill="green";
	movePacket2_1();
	animate1ID = setInterval(movePacket2_1, 5000);
	animateBarID = setInterval(moveBar, 400);
	animateGear2_2.beginElement();
}

function animate2_2(){

	animateGear2_2.endElement();
	animateGear2_1.beginElement();

	clearInterval(animate1ID);
	clearInterval(animateBarID);

	//$("#packet2_1").velocity("stop");
	animate2ID = setInterval(movePacket2_1_2, 5000);
}

function animate2_3(){
	//		clearInterval(animate2ID);
	animateGear2_1.endElement();
	question2_1.style.visibility="visible";
}

function stop2(){
	clearInterval(animate1ID);
	clearInterval(animate2ID);
	clearInterval(animateBarID);
	animateGear1.setAttribute('dur', '4s');
	node1.style.fill="white";
}

function moveBar(){

	$("#bar1_2")
		.velocity({ width: "+=0.2" });

	//		console.log("Width: " + bar1_2.getBBox().width);
	if(bar1_2.getBBox().width > 13){
		clearInterval(animateBarID);
	}
}



function movePacket2_1(){
	$("#packet2_1")
		.velocity({translateX: 0, translateY: 0}, 500)
		.velocity({opacity: 1}, { duration: 1000 }, {visibility: "visible"})
		.velocity({translateX: 20, translateY: 0}, packetInterval)
		.velocity({translateX: 54, translateY: -21}, packetInterval)
		.velocity({translateX: 57, translateY: -60}, packetInterval)
		.velocity({ opacity: 0 }, {duration: 500}, {visibility: "hidden"});

}

function movePacket2_1_2(){
	$("#packet2_1")
		.velocity({translateX: 0, translateY: 0}, 500)
		.velocity({opacity: 1}, { duration: 1000 }, {visibility: "visible"})
		.velocity({translateX: 20, translateY: 0}, packetInterval)
		.velocity({translateX: 54, translateY: -21}, packetInterval)
		.velocity({translateX: 102, translateY: -17}, packetInterval)
		.velocity({ opacity: 0 }, {duration: 500}, {visibility: "hidden"});

}


//~~~~~~~~~~~~~~~~~~SLIDE 3~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

var animate3_1ID;
var animate3_1_3ID;
var animate3_2ID;
var packetInterval=700;

function animate3_1(){
	packet3_1.style.visibility="visible";
	node3_1.style.fill="green";
	movePacket3_1();
	animate3_1ID = setInterval(movePacket3_1, 5000);
}

function animate3_2(){
	packet3_2.style.visibility="visible";
	node3_11.style.fill="green";
	movePacket3_2();
	animate3_2ID = setInterval(movePacket3_2, 6000);
}

function animate3_3(){

	clearInterval(animate3_1ID);
	clearInterval(animate3_2ID);


	$("#gear3_2")
		.velocity({translateX: -59, translateY: -7}, 1500);

	$("#gear3_3")
		.delay(2000)
		.velocity({translateX: -66, translateY: 74}, 1500);


}

function animate3_4(){
	gear3_2.style.visibility="hidden";
	gear3_3.style.visibility="hidden";
	gear3_2_2.style.visibility="visible";
	gear3_3_2.style.visibility="visible";

	animate3_1ID = setInterval(movePacket3_1_2, 4000);
	animate3_2ID = setInterval(movePacket3_2_2, 4000);

	animateGear3_1.endElement();
}

function animate3_5(){
	node3_1.style.fill="red";
	animateGear3_2_2.setAttribute('dur', '1s');
	animate3_1_3ID = setInterval(movePacket3_1_3, 2000);

}


function animate3_6(){
	gear3_4.style.visibility="visible";

	$("#gear3_4")
		.velocity({translateX: 27, translateY: 13}, 1500);

	setTimeout(appear3, 1500);

}

function appear3(){
	gear3_4_2.style.visibility="visible";
	gear3_4.style.visibility="hidden";
	node3_1.style.fill="green";
	animateGear3_2_2.setAttribute('dur', '5s');
}



function movePacket3_1(){
	$("#packet3_1")
		.velocity({translateX: 0, translateY: 0}, 500)
		.velocity({opacity: 1}, { duration: 1000 }, {visibility: "visible"})
		.velocity({translateX: 21, translateY: 0}, packetInterval)
		.velocity({translateX: 48, translateY: 13}, packetInterval)
		.velocity({translateX: 80, translateY: 7}, packetInterval)
		.velocity({ opacity: 0 }, {duration: 500}, {visibility: "hidden"});

}

function movePacket3_1_2(){
	$("#packet3_1")
		.velocity({translateX: 0, translateY: 0}, 500)
		.velocity({opacity: 1}, { duration: 1000 }, {visibility: "visible"})
		.velocity({translateX: 21, translateY: 0}, packetInterval)
		.velocity({ opacity: 0 }, {duration: 500}, {visibility: "hidden"});

}

function movePacket3_1_3(){
	$("#packet3_1")
		.velocity({translateX: 0, translateY: 0}, 500)
		.velocity({opacity: 1}, { duration: 1000 }, {visibility: "visible"})
		.velocity({translateX: 21, translateY: 0}, packetInterval)
		.velocity({translateX: 48, translateY: 13}, packetInterval)
		.velocity({ opacity: 0 }, {duration: 500}, {visibility: "hidden"});

}

function movePacket3_2(time){
	$("#packet3_2")
		.velocity({translateX: 0, translateY: 0}, 500)
		.velocity({opacity: 1}, { duration: 1000 }, {visibility: "visible"})
		.velocity({translateX: 17, translateY: 0}, packetInterval)
		.velocity({translateX: 47, translateY: 1}, packetInterval)
		.velocity({translateX: 73, translateY: -18}, packetInterval)
		.velocity({translateX: 83, translateY: -44}, packetInterval)
		.velocity({translateX: 83, translateY: -74}, packetInterval)
		.velocity({ opacity: 0 }, {duration: 500}, {visibility: "hidden"});
}
function movePacket3_2_2(time){
	$("#packet3_2")
		.velocity({translateX: 0, translateY: 0}, 500)
		.velocity({opacity: 1}, { duration: 1000 }, {visibility: "visible"})
		.velocity({translateX: 17, translateY: 0}, packetInterval)
		.velocity({ opacity: 0 }, {duration: 500}, {visibility: "hidden"});
}



//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SLIDE 4~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

var animate4_1ID;
var animate4_2ID;
var animate4_3ID;
var animate4_4ID;
var animate4_5ID;


function animate4_1(){
	packet4_1.style.visibility="visible";
	node4_3.style.fill="green";
	movePacket4_1();
	animate4_1ID = setInterval(movePacket4_1, 5000);
}

function animate4_2(){
	clearInterval(animate4_1ID);
	$("#packet4_1").velocity("stop");

	animate4_2ID = setInterval(movePacket4_2, 5000);
	packet4_2.style.visibility="visible";
	packet4_3.style.visibility="visible";
    packet4_4.style.visibility="visible";

}

function animate4_3(){
		clearInterval(animate4_2ID);
		animateGear4_1.endElement();
		gear4_2.style.visibility="visible";
		gear4_3.style.visibility="visible";

		node4_6.style.fill="red";
		node4_7.style.fill="red";

		$("#gear4_2")
			.velocity({translateX: -63, translateY: 4}, 1500);

		$("#gear4_3")
			.delay(1000)
			.velocity({translateX: -41, translateY: 54}, 1500)

}

function animate4_4(){
		gear4_4.style.visibility="visible";
		gear4_5.style.visibility="visible";
		gear4_2.style.visibility="hidden";
		gear4_3.style.visibility="hidden";
		animateGear4_4.beginElement();
		animate4_ID = setInterval(movePacket4_3, 4000);



		node4_6.style.fill="white";
		node4_7.style.fill="white";
}

function animate4_5(){
	$("#gear4_5")
		.velocity({ opacity: 0 }, {duration: 500}, {visibility: "hidden"});

}

function animate4_6(){
	packet4_2.style.visibility="visible";
	packet4_3.style.visibility="visible";
	packet4_4.style.visibility="visible";
	node4_8.style.fill="orange";
	node4_9.style.fill="blue";
	gear4_5.style.visibility="visible";
	packet4_5.style.visibility="visible";
	$("#gear4_5")
		.velocity({ opacity: 0 }, {duration: 500}, {visibility: "visible"});
	animate4_ID = setInterval(movePacket4_4, 4000);


}

function movePacket4_1(){
	$("#packet4_1")
		.velocity({translateX: 0, translateY: 0}, 500)
		.velocity({opacity: 1}, { duration: 1000 }, {visibility: "visible"})
		.velocity({translateX: 20, translateY: 0}, packetInterval)
		.velocity({translateX: 58, translateY: -2}, packetInterval)
		.velocity({translateX: 88, translateY: -12}, packetInterval)
		.velocity({translateX: 88.5, translateY: -30}, packetInterval)
		.velocity({ opacity: 0 }, {duration: 500}, {visibility: "hidden"});
}


function movePacket4_2(){

	$("#packet4_1")
		.velocity({translateX: 0, translateY: 0}, 0)
		.velocity({opacity: 1}, { duration: 1000 }, {visibility: "visible"})
		.velocity({translateX: 20, translateY: 0}, packetInterval)
		.velocity({translateX: 58, translateY: -2}, packetInterval)
		.velocity({translateX: 88, translateY: -12}, packetInterval)
		.velocity({translateX: 88.5, translateY: -30}, packetInterval)
		.velocity({ opacity: 0 }, {duration: 500}, {visibility: "hidden"});

	$("#packet4_2")
		.velocity({translateX: 0, translateY: 0}, 0)
		.velocity({opacity: 1}, { duration: 1000 }, {visibility: "visible"})
		.velocity({translateX: 20, translateY: 0}, packetInterval)
		.velocity({translateX: 26, translateY: -26}, packetInterval)
		.velocity({ opacity: 0 }, {duration: 500}, {visibility: "hidden"});
	$("#packet4_3")
		.velocity({translateX: 0, translateY: 0}, 0)
		.velocity({opacity: 1}, { duration: 1000 }, {visibility: "visible"})
		.velocity({translateX: 20, translateY: 0}, packetInterval)
		.velocity({translateX: 1, translateY: 22}, packetInterval)
		.velocity({ opacity: 0 }, {duration: 500}, {visibility: "hidden"});
	$("#packet4_4")
		.velocity({translateX: 0, translateY: 0}, 0)
		.velocity({opacity: 1}, { duration: 1000 }, {visibility: "visible"})
		.velocity({translateX: 20, translateY: 0}, packetInterval)
		.velocity({translateX: 48, translateY: 24}, packetInterval)
		.velocity({ opacity: 0 }, {duration: 500}, {visibility: "hidden"});

}

function movePacket4_3(){
	$("#packet4_1")
		.velocity({translateX: 0, translateY: 0}, 500)
		.velocity({opacity: 1}, { duration: 1000 }, {visibility: "visible"})
		.velocity({translateX: 20, translateY: 0}, packetInterval)
		.velocity({translateX: 26, translateY: -26}, packetInterval)
		.velocity({ opacity: 0 }, {duration: 500}, {visibility: "hidden"});
}

function movePacket4_4(){
	$("#packet4_5")
		.velocity({translateX: 0, translateY: 0}, 0)
		.velocity({opacity: 1}, { duration: 1000 }, {visibility: "visible"})
		.velocity({translateX: 20, translateY: 0}, packetInterval)
		.velocity({translateX: 48, translateY: 24}, packetInterval)
		.velocity({ opacity: 0 }, {duration: 500}, {visibility: "hidden"});
}
